package com.example.miniMarket.controllers;

import com.example.miniMarket.models.dtos.*;
import com.example.miniMarket.models.entities.StockEntity;
import com.example.miniMarket.service.StockService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/stock")
public class StockController {
    private final StockService stockService;

    public StockController(StockService stockService) {
        this.stockService = stockService;
    }
    @PostMapping("/new")
    public ResponseEntity<String> createStock(@RequestBody NewStockDTO newStockDTO) {
        return stockService.createStock(newStockDTO);

    }
    @PutMapping("/edit/{IdStock}")
    public ResponseEntity<?> updateStock(@RequestBody NewStockDTO newStockDTO, @PathVariable Long IdStock) {
        return  stockService.updateStock(newStockDTO, IdStock);
    }
    @GetMapping("/{IdStock}")
    public ResponseEntity<StocksDTO> getStock (@PathVariable Long IdStock) {
        return new ResponseEntity <> ( stockService.getStock(IdStock), HttpStatus.OK);
    }
    @GetMapping("/all")
    public ResponseEntity<StocksDTO > getStocks () {
        return new ResponseEntity <> ( stockService.getStocks(), HttpStatus.OK);
    }
    @DeleteMapping("/{IdStock}")
    public ResponseEntity<String> deleteStock (@PathVariable Long IdStock) {
        return  stockService.deleteStock (IdStock);

    }
}
