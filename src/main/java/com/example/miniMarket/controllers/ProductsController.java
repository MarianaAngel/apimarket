package com.example.miniMarket.controllers;

import com.example.miniMarket.models.dtos.*;
import com.example.miniMarket.models.entities.ProductsEntity;
import com.example.miniMarket.service.ProductsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductsController {
    private final ProductsService productsService;

    public ProductsController(ProductsService productsService) {
        this.productsService = productsService;
    }
    @PostMapping("/new")
    public ResponseEntity<String> createProducts(@RequestBody NewProductsDTO newProductsDTO) {
        return  productsService.createProducts(newProductsDTO);

    }
    @GetMapping("/{IdProduct}")
    public ResponseEntity<ProductsDTO> getProduct (@PathVariable Long IdProduct) {
        return new ResponseEntity <> ( productsService.getProduct(IdProduct), HttpStatus.OK);
    }
    @GetMapping("/all")
    public ResponseEntity<ProductsDTO> getProducts () {
        return new ResponseEntity <> ( productsService.getProducts(), HttpStatus.OK);
    }
    @DeleteMapping("/{IdProduct}")
    public ResponseEntity<String> deleteCategories (@PathVariable Long IdProduct) {
        return  productsService.deleteProduct (IdProduct);

    }
    @PutMapping("/edit/{IdProduct}")
    public ResponseEntity<?> updateProduct(@RequestBody NewProductsDTO newProductsDTO, @PathVariable Long IdProduct) {
        return  productsService.updateProduct(newProductsDTO, IdProduct);

    }
}
