package com.example.miniMarket.controllers;

import com.example.miniMarket.models.dtos.*;
import com.example.miniMarket.service.UsersService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UsersController {
    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @PostMapping("/new")
    public ResponseEntity<String> createUser(@RequestBody NewUserDTO newUserDTO) {
        return usersService.createUser(newUserDTO);

    }
    @GetMapping("/all")
    public ResponseEntity<UsersDTO> getUsers () {
        return new ResponseEntity <> (usersService.getUsers(), HttpStatus.OK);
    }
    @GetMapping("/{IdUser}")
    public ResponseEntity<UsersDTO> getUser (@PathVariable Long IdUser) {
        return new ResponseEntity <> ( usersService.getUser(IdUser), HttpStatus.OK);
    }
    @PutMapping("/edit/{IdUser}")
    public ResponseEntity<?> updateUser(@RequestBody NewUserDTO newUserDTO, @PathVariable Long IdUser) {
        return  usersService.updateUser(newUserDTO, IdUser);

    }
    @DeleteMapping("/{IdUser}")
    public ResponseEntity<String> deleteUser (@PathVariable Long IdUser) {
        return  usersService.deleteUser (IdUser);

    }
}
