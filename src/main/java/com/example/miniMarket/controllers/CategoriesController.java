package com.example.miniMarket.controllers;

import com.example.miniMarket.models.dtos.*;
import com.example.miniMarket.service.CategoriesService;
import com.example.miniMarket.service.StockService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/categories")
public class CategoriesController {
    private final  CategoriesService categoriesService;

    public CategoriesController(CategoriesService categoriesService) {
        this.categoriesService = categoriesService;
    }

    @PostMapping("/new")
    public ResponseEntity<String> createCategories(@RequestBody NewCategoryDTO newCategoryDTO) {
        return  categoriesService.createCategories(newCategoryDTO);

    }
    @PutMapping("/edit/{IdCategory}")
    public ResponseEntity<?> updateCategory(@RequestBody NewCategoryDTO newCategoryDTO, @PathVariable Long IdCategory) {
        return  categoriesService.updateCategories(newCategoryDTO, IdCategory);

    }
    @GetMapping("/{IdCategory}")
    public ResponseEntity<CategoriesDTO> getCategory (@PathVariable Long IdCategory) {
        return new ResponseEntity <> ( categoriesService.getCategory(IdCategory), HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<CategoriesDTO> getCategories () {
        return new ResponseEntity <> ( categoriesService.getCategories(), HttpStatus.OK);
    }
    @DeleteMapping("/{IdCategory}")
    public ResponseEntity<String> deleteCategories (@PathVariable Long IdCategory) {
        return  categoriesService.deleteCategory (IdCategory);

    }


}
