package com.example.miniMarket.controllers;

import com.example.miniMarket.models.dtos.NewSubsidiariesDTO;
import com.example.miniMarket.models.dtos.NewUserDTO;
import com.example.miniMarket.models.dtos.SubsidiariesDTO;
import com.example.miniMarket.models.dtos.SubsidiaryDTO;
import com.example.miniMarket.service.SubsidiariesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/subsidiaries")
public class SubsidiariesController {
    private final SubsidiariesService subsidiariesService;

    public SubsidiariesController(SubsidiariesService subsidiariesService) {
        this.subsidiariesService = subsidiariesService;
    }
    @PostMapping("/new")
    public ResponseEntity<String> createSubsidiaries(@RequestBody NewSubsidiariesDTO newSubsidiariesDTO) {
        return  subsidiariesService.createSubsidiaries(newSubsidiariesDTO);

    }
    @GetMapping("/{IdSubsidiary}")
    public ResponseEntity<SubsidiariesDTO> getSubsidiary (@PathVariable Long IdSubsidiary) {
        return new ResponseEntity <> (subsidiariesService.getSubsidiary(IdSubsidiary), HttpStatus.OK);
    }
    @GetMapping("/all")
    public ResponseEntity<SubsidiariesDTO> getSubsidiaries () {
        return new ResponseEntity <> (subsidiariesService.getSubsidiaries(), HttpStatus.OK);
    }
    @DeleteMapping("/{IdSubsidiary}")
    public ResponseEntity<String> deleteSubsidiaries (@PathVariable Long IdSubsidiary) {
        return  subsidiariesService.deleteSubsidiaries (IdSubsidiary);

    }
    @PutMapping("/edit/{IdSubsidiary}")
    public ResponseEntity<?> updateSubsidiaries(@RequestBody NewSubsidiariesDTO newSubsidiariesDTO, @PathVariable Long IdSubsidiary) {
        return  subsidiariesService.UpdateSubsidiaries(newSubsidiariesDTO, IdSubsidiary);

    }
}
