package com.example.miniMarket.service;

import com.example.miniMarket.excepctions.BadRequestException;
import com.example.miniMarket.models.dtos.*;
import com.example.miniMarket.models.entities.CategoriesEntity;
import com.example.miniMarket.models.entities.SubsidiariesEntity;
import com.example.miniMarket.models.mappers.CategoriesMapper;
import com.example.miniMarket.models.repositories.CategoriesRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoriesService {

    private final CategoriesRepository categoriesRepository;
    private final CategoriesMapper categoriesMapper;

    public CategoriesService(CategoriesRepository categoriesRepository, CategoriesMapper categoriesMapper) {
        this.categoriesRepository = categoriesRepository;
        this.categoriesMapper = categoriesMapper;
    }

    public ResponseEntity<String> createCategories(NewCategoryDTO newCategoryDTO) {

        CategoriesEntity categoriesEntity = categoriesMapper.mapCategoryDtoToCategoriesEntity(newCategoryDTO);
        categoriesRepository.save(categoriesEntity);

        return new ResponseEntity<>("Transaccion Creada", HttpStatus.OK);
    }
    public ResponseEntity<String> updateCategories(NewCategoryDTO newCategoryDTO,Long  id) {
      if(!categoriesRepository.existsById(id) ){
          throw new BadRequestException("Error al crear la transacción, no existe Id  ");
      } else {
                  CategoriesEntity categoriesEntity = categoriesRepository.getOne(id);
                  categoriesEntity.setName(newCategoryDTO.getName());
                  categoriesRepository.save(categoriesEntity);

                  return new ResponseEntity<>("Transaccion Creada", HttpStatus.OK);
              }


    }
    public CategoriesDTO getCategory(Long id) {

        Iterable<CategoriesEntity> categoriesEntities = categoriesRepository.findAllById(id);
        List<CategoryDTO> CategoryDTOList =  new ArrayList<>();

        for (CategoriesEntity categoriesEntity : categoriesEntities) {

            CategoryDTOList.add(new CategoryDTO(categoriesEntity.getName()));
        }
        return new CategoriesDTO(CategoryDTOList);
    }
    public CategoriesDTO getCategories() {
        Iterable<CategoriesEntity> categoriesEntities = categoriesRepository.findAll();
        List<CategoryDTO> CategoryDTOList = new ArrayList<>();

        for (CategoriesEntity categoriesEntity : categoriesEntities) {

            CategoryDTOList.add(
                    new CategoryDTO(categoriesEntity.getName())
            );

        }
        return new CategoriesDTO(CategoryDTOList);
    }
    public ResponseEntity<String> deleteCategory(Long id) {
        boolean existCategory = categoriesRepository.existsById(id);
        Optional<CategoriesEntity> categoriesEntity=categoriesRepository.findById(id);
        if (existCategory) {
            categoriesRepository.delete(categoriesEntity.get());
            return new ResponseEntity<>("Operación realizada con éxito", HttpStatus.OK);

        } else {
            throw new BadRequestException("No existe el Id ");
        }

    }

}
