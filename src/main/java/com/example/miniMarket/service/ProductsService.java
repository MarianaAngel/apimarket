package com.example.miniMarket.service;

import com.example.miniMarket.excepctions.BadRequestException;
import com.example.miniMarket.models.dtos.*;
import com.example.miniMarket.models.entities.BrandsEntities;
import com.example.miniMarket.models.entities.CategoriesEntity;
import com.example.miniMarket.models.entities.ProductsEntity;
import com.example.miniMarket.models.entities.SubsidiariesEntity;
import com.example.miniMarket.models.mappers.ProductsMapper;
import com.example.miniMarket.models.repositories.BrandsRepository;
import com.example.miniMarket.models.repositories.CategoriesRepository;
import com.example.miniMarket.models.repositories.ProductsRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductsService {
    private final ProductsRepository productsRepository;
    private final ProductsMapper productsMapper;
    private final CategoriesRepository categoriesRepository;
    private final BrandsRepository brandsRepository;

    public ProductsService(ProductsRepository productsRepository, ProductsMapper productsMapper, CategoriesRepository categoriesRepository, BrandsRepository brandsRepository) {
        this.productsRepository = productsRepository;
        this.productsMapper = productsMapper;
        this.categoriesRepository = categoriesRepository;
        this.brandsRepository = brandsRepository;
    }

    public ResponseEntity<String> createProducts(NewProductsDTO newProductsDTO) {

        ProductsEntity productsEntity = productsMapper.mapProductsDtoToProductsEntity(newProductsDTO);
        boolean existCategory = categoriesRepository.existsById(newProductsDTO.getCategory_id());
        boolean existBrand = brandsRepository.existsById(newProductsDTO.getBrand_id());
        boolean validate = true;
        String message = "";
        if (!existCategory) {
            validate = false;
            message = "No existe la categoría";
        }
        if (!existBrand) {
            validate = false;
            message = "No existe la marca";
        }
        if (validate) {
            productsRepository.save(productsEntity);
            return new ResponseEntity<>("Transaccion Creada", HttpStatus.OK);
        }
        else {
            throw new BadRequestException("Error al crear la transacción "+message);
        }
    }
    public ProductsDTO getProduct(Long id) {
        Optional<ProductsEntity> productsEntities = productsRepository.findById(id);
        List<ProductDTO> ProductsDTOList = new ArrayList<>();

        for (ProductsEntity productsEntity : productsEntities.stream().toList() ){
            Optional<CategoriesEntity> ToCategoryName=categoriesRepository.findById(productsEntity.getCategory_id()) ;
            Optional<BrandsEntities> ToBrandyName=brandsRepository.findById(productsEntity.getBrand_id()) ;
            ProductsDTOList.add(
                    new ProductDTO(productsEntity.getName(),productsEntity.getObservations(), productsEntity.getMeasure_unit(), productsEntity.getUnit_amount(),
                            ToCategoryName.get().getName(),ToBrandyName.get().getName())
            );

        }
        return new ProductsDTO(ProductsDTOList);
    }
    public ProductsDTO getProducts( ) {
        Iterable<ProductsEntity> productsEntities = productsRepository.findAll();
        List<ProductDTO> ProductsDTOList = new ArrayList<>();

        for (ProductsEntity productsEntity : productsEntities ){
            Optional<CategoriesEntity> ToCategoryName=categoriesRepository.findById(productsEntity.getCategory_id()) ;
            Optional<BrandsEntities> ToBrandyName=brandsRepository.findById(productsEntity.getBrand_id()) ;
            ProductsDTOList.add(
                    new ProductDTO(productsEntity.getName(),productsEntity.getObservations(), productsEntity.getMeasure_unit(), productsEntity.getUnit_amount(),
                            ToCategoryName.get().getName(),ToBrandyName.get().getName())
            );

        }
        return new ProductsDTO(ProductsDTOList);
    }
    public ResponseEntity<String> deleteProduct(Long id) {
        boolean existProduct = productsRepository.existsById(id);
        Optional<ProductsEntity> productsEntity=productsRepository.findById(id);
        if (existProduct) {
            productsRepository.delete(productsEntity.get());
            return new ResponseEntity<>("Operación realizada con éxito", HttpStatus.OK);

        } else {
            throw new BadRequestException("No existe el Id ");
        }

    }
    public ResponseEntity<String> updateProduct(NewProductsDTO  newProductsDTO,Long  id) {
         boolean validate =true;
         String message="";
        if(!productsRepository.existsById(id) ){
            validate =false;
            message="NO existe el producto";
        }
        if (!categoriesRepository.existsById(newProductsDTO.getCategory_id() )){
            validate =false;
            message="NO existe la categoria";
        }
        if (!brandsRepository.existsById(newProductsDTO.getBrand_id() )){
            validate =false;
            message="NO existe la marca";
        }
        if(!validate ){
            throw new BadRequestException("Error al modificar la transacción "+message);
        } else {
            ProductsEntity productsEntity = productsRepository.getOne(id);
            productsEntity.setName(newProductsDTO.getName());
            productsEntity.setObservations(newProductsDTO.getObservations());
            productsEntity.setCategory_id(newProductsDTO.getCategory_id());
            productsEntity.setBrand_id(newProductsDTO.getBrand_id());
            productsEntity.setMeasure_unit(newProductsDTO.getMeasure_unit());
            productsEntity.setUnit_amount(newProductsDTO.getUnit_amount());

            productsRepository.save(productsEntity);

            return new ResponseEntity<>("Transaccion modificada", HttpStatus.OK);
        }


    }

}
