package com.example.miniMarket.service;

import com.example.miniMarket.excepctions.BadRequestException;
import com.example.miniMarket.models.dtos.NewSubsidiariesDTO;
import com.example.miniMarket.models.dtos.SubsidiariesDTO;
import com.example.miniMarket.models.dtos.SubsidiaryDTO;
import com.example.miniMarket.models.entities.CategoriesEntity;
import com.example.miniMarket.models.entities.SubsidiariesEntity;
import com.example.miniMarket.models.mappers.SubsidiariesMapper;
import com.example.miniMarket.models.repositories.SubsidiariesRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SubsidiariesService {
    private final SubsidiariesRepository subsidiariesRepository;
    private final SubsidiariesMapper subsidiariesMapper;

    public SubsidiariesService(SubsidiariesRepository subsidiariesRepository, SubsidiariesMapper subsidiariesMapper) {
        this.subsidiariesRepository = subsidiariesRepository;
        this.subsidiariesMapper = subsidiariesMapper;
    }

    public ResponseEntity<String> createSubsidiaries(NewSubsidiariesDTO newSubsidiariesDTO) {

        SubsidiariesEntity subsidiariesEntity = subsidiariesMapper.mapSubsidiariesDtoToSubsidiariesEntity(newSubsidiariesDTO);
        subsidiariesRepository.save(subsidiariesEntity);

        return new ResponseEntity<>("Transaccion Creada", HttpStatus.OK);
    }

    public SubsidiariesDTO getSubsidiary(Long id) {
        List<SubsidiariesEntity> subsidiariesEntities = subsidiariesRepository.findAllById(id);
        List<SubsidiaryDTO> SubsidiaryDTOList = new ArrayList<>();

        for (SubsidiariesEntity subsidiariesEntity : subsidiariesEntities) {

            SubsidiaryDTOList.add(
                    new SubsidiaryDTO(subsidiariesEntity.getName(),
                            subsidiariesEntity.getAddress(),
                            subsidiariesEntity.getCity())
            );


        }
        return new SubsidiariesDTO(SubsidiaryDTOList);
    }

    public SubsidiariesDTO getSubsidiaries() {
        Iterable<SubsidiariesEntity> subsidiariesEntities = subsidiariesRepository.findAll();
        List<SubsidiaryDTO> SubsidiaryDTOList = new ArrayList<>();

        for (SubsidiariesEntity subsidiariesEntity : subsidiariesEntities) {

            SubsidiaryDTOList.add(
                    new SubsidiaryDTO(subsidiariesEntity.getName(),
                            subsidiariesEntity.getAddress(),
                            subsidiariesEntity.getCity())
            );


        }
        return new SubsidiariesDTO(SubsidiaryDTOList);
    }

    public ResponseEntity<String> deleteSubsidiaries(Long id) {
        boolean existSubsidiary = subsidiariesRepository.existsById(id);
        Optional<SubsidiariesEntity> subsidiariesEntity=subsidiariesRepository.findById(id);
        if (existSubsidiary) {
            subsidiariesRepository.delete(subsidiariesEntity.get());
            return new ResponseEntity<>("Operación realizada con éxito", HttpStatus.OK);

        } else {
            throw new BadRequestException("No existe el Id ");
        }

    }
    public ResponseEntity<?> UpdateSubsidiaries(NewSubsidiariesDTO newSubsidiariesDTO, Long id) {
        boolean exist= subsidiariesRepository.existsById(id);
        if (!exist) {
            throw new BadRequestException("Error al crear la transacción, no existe Id ");
        }else{

            SubsidiariesEntity subsidiariesEntity= subsidiariesRepository.getOne(id);
            subsidiariesEntity.setName(newSubsidiariesDTO.getName());
            subsidiariesEntity.setAddress(newSubsidiariesDTO.getAddress());
            subsidiariesEntity.setCity (newSubsidiariesDTO.getCity());
            subsidiariesRepository.save(subsidiariesEntity);
            return new ResponseEntity<>("Transaccion Creada", HttpStatus.OK);
        }

    }




}
 