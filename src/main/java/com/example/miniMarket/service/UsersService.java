package com.example.miniMarket.service;


import com.example.miniMarket.excepctions.BadRequestException;
import com.example.miniMarket.models.dtos.*;
import com.example.miniMarket.models.entities.*;
import com.example.miniMarket.models.mappers.UserMapper;
import com.example.miniMarket.models.repositories.SubsidiariesRepository;
import com.example.miniMarket.models.repositories.UsersRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsersService {
    private final UserMapper userMapper;
    private final UsersRepository usersRepository;
    private final SubsidiariesRepository subsidiariesRepository;
    public UsersService(UserMapper userMapper, UsersRepository usersRepository, SubsidiariesRepository subsidiariesRepository) {
        this.userMapper = userMapper;
        this.usersRepository = usersRepository;
        this.subsidiariesRepository = subsidiariesRepository;
    }
    public ResponseEntity<String> createUser(NewUserDTO newUserDTO) {

        UserEntity userEntity = userMapper.mapUserDtoToUserEntity(newUserDTO);
        boolean existSubsidiary=subsidiariesRepository.existsById(newUserDTO.getSubsidiary_id() );
        boolean existUser = usersRepository.existsByUsername(newUserDTO.getUsername());
        boolean validate=true;
        String message="";
        if (!existSubsidiary) {
            validate=false;
            message="El Id de sucursal no existe";
        }
        if (existUser){
            validate=false;
            message="El Usuario existe";
        }
        if (validate) {
           usersRepository.save(userEntity);

            return new ResponseEntity<>("Transaccion Creada", HttpStatus.OK);
        } else {
            throw new BadRequestException("Error al crear la transacción "+message);
        }

    }
    public UsersDTO getUsers() {
        Iterable<UserEntity> userEntities = usersRepository.findAll();
        List<UserDTO> UserDtoList = new ArrayList<>();

        for (UserEntity userEntity : userEntities) {
            Long Id_ =userEntity.getSubsidiary_id();
       Optional<SubsidiariesEntity> ToSubsidiaryName=subsidiariesRepository.findById(Id_) ;
             UserDtoList.add(
                    new UserDTO(userEntity.getFirstName(),userEntity.getLastName(),userEntity.getAddress(),
                            ToSubsidiaryName.get().getName(), userEntity.getUsername()));



        }
        return new UsersDTO(UserDtoList);
    }
    public UsersDTO getUser(Long id) {
        Optional<UserEntity> userEntities = usersRepository.findById(id);
        List<UserDTO> UserDTOList = new ArrayList<>();

        for (UserEntity userEntity : userEntities.stream().toList() ){
            Long Id_ =userEntity.getSubsidiary_id();
            Optional<SubsidiariesEntity> ToSubsidiaryName=subsidiariesRepository.findById(Id_) ;
            UserDTOList.add(
                    new UserDTO(userEntity.getFirstName(),userEntity.getLastName(),userEntity.getAddress(),
                            ToSubsidiaryName.get().getName(), userEntity.getUsername()));

        }
        return new UsersDTO(UserDTOList);
    }
    public ResponseEntity<String> updateUser(NewUserDTO  newUserDTO,Long  id) {
        boolean validate =true;
        String message="";
        if(!usersRepository.existsById(id) ){
            validate =false;
            message="NO existe el usuario";
        }
        if (!subsidiariesRepository.existsById(newUserDTO.getSubsidiary_id() )){
            validate =false;
            message="NO existe la sucursal";
        }

        if(!validate ){
            throw new BadRequestException("Error al modificar la transacción "+message);
        } else {
            UserEntity userEntity = usersRepository.getOne(id);
            userEntity.setFirstName(newUserDTO.getFirstName());
            userEntity.setLastName(newUserDTO.getLastName());
            userEntity.setSubsidiary_id(newUserDTO.getSubsidiary_id());
            userEntity.setAddress(newUserDTO.getAddress());
            userEntity.setUsername(newUserDTO.getUsername());
            userEntity.setPassword(newUserDTO.getPassword());
            usersRepository.save(userEntity);

            return new ResponseEntity<>("Transaccion modificada", HttpStatus.OK);
        }


    }
    public ResponseEntity<String> deleteUser(Long id) {
        boolean existUser = usersRepository.existsById(id);
        Optional<UserEntity> userEntity=usersRepository.findById(id);
        if (existUser) {
            usersRepository.delete(userEntity.get());
            return new ResponseEntity<>("Operación realizada con éxito", HttpStatus.OK);

        } else {
            throw new BadRequestException("No existe el Id ");
        }

    }

}
