package com.example.miniMarket.service;

import com.example.miniMarket.excepctions.BadRequestException;
import com.example.miniMarket.models.dtos.*;
import com.example.miniMarket.models.entities.ProductsEntity;
import com.example.miniMarket.models.entities.StockEntity;
import com.example.miniMarket.models.entities.SubsidiariesEntity;
import com.example.miniMarket.models.entities.UserEntity;
import com.example.miniMarket.models.mappers.StockMapper;
import com.example.miniMarket.models.repositories.ProductsRepository;
import com.example.miniMarket.models.repositories.StockRepository;
import com.example.miniMarket.models.repositories.SubsidiariesRepository;
import com.example.miniMarket.models.repositories.UsersRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StockService {
    private final StockRepository stockRepository;
    private final StockMapper stockMapper;
    private final UsersRepository usersRepository;
    private final ProductsRepository productsRepository;
    private final SubsidiariesRepository subsidiariesRepository;

    public StockService(StockRepository stockRepository, StockMapper stockMapper, UsersRepository usersRepository, ProductsRepository productsRepository, SubsidiariesRepository subsidiariesRepository) {
        this.stockRepository = stockRepository;
        this.stockMapper = stockMapper;
        this.usersRepository = usersRepository;
        this.productsRepository = productsRepository;
        this.subsidiariesRepository = subsidiariesRepository;
    }
    public ResponseEntity<String> createStock(NewStockDTO newStockDTO) {
        StockEntity stockEntity = stockMapper.mapStockDtoToStockEntity(newStockDTO);
        boolean validate =true;
        String message="";
        if (newStockDTO.getUser_id().equals(null)) {

            message="El usuario es nulo";
            throw new BadRequestException("Error al crear la transacción "+message);

        }

        if (!usersRepository.existsById(newStockDTO.getUser_id())){

            message="No existe usuario";
            throw new BadRequestException("Error al crear la transacción "+message);
        }
        if(!subsidiariesRepository.existsById(newStockDTO.getSubsidiary_id())){

            message="No existe la sucursal";
            throw new BadRequestException("Error al crear la transacción "+message);
        }

        if (!productsRepository.existsById(newStockDTO.getProduct_id())){
            message="No existe el Producto";
            throw new BadRequestException("Error al crear la transacción "+message);
        }

        stockRepository.save(stockEntity);

        return new ResponseEntity<>("Transaccion Creada", HttpStatus.OK);
    }
    public ResponseEntity<String> updateStock(NewStockDTO newStockDTO, Long  id) {
        boolean validate =true;
        String message="";
        if (!stockRepository.existsById(id)){
            message="NO existe el Id de Stock";
            throw new BadRequestException("Error al crear la transacción "+message);
        }
        if(!productsRepository.existsById(newStockDTO.getProduct_id()) ){

            message="NO existe el producto";
            throw new BadRequestException("Error al crear la transacción "+message);
        }
        if (!usersRepository.existsById(newStockDTO.getUser_id())){

            message="No existe usuario";
            throw new BadRequestException("Error al crear la transacción "+message);
        }
        if(!subsidiariesRepository.existsById(newStockDTO.getSubsidiary_id())){

            message="No existe la sucursal";
            throw new BadRequestException("Error al crear la transacción "+message);
        }

            StockEntity stockEntity = stockRepository.getOne(id);
            stockEntity.setProduct_id(newStockDTO.getProduct_id());
            stockEntity.setSubsidiary_id(newStockDTO.getSubsidiary_id());
            stockEntity.setUser_id(newStockDTO.getUser_id());
            stockEntity.setObservations(newStockDTO.getObservations());
            stockEntity.setQuantity(newStockDTO.getQuantity());

            stockRepository.save(stockEntity);

            return new ResponseEntity<>("Transaccion modificada", HttpStatus.OK);

    }
    public StocksDTO getStock(Long id) {
        Optional<StockEntity> stockEntities = stockRepository.findById(id);
        List<StockDTO> StockDTOList = new ArrayList<>();

        for (StockEntity stockEntity : stockEntities.stream().toList() ){
            Optional<ProductsEntity>ToProductName=productsRepository.findById(stockEntities.get().getProduct_id());
            Optional<SubsidiariesEntity> ToSubsidiaryName=subsidiariesRepository.findById(stockEntities.get().getSubsidiary_id()) ;
            Optional<UserEntity>ToUserName=usersRepository.findById(stockEntities.get().getUser_id());
            StockDTOList.add(
                    new StockDTO(ToProductName.get().getName(), ToSubsidiaryName.get().getName(),ToUserName.get().getUsername(),stockEntities.get().getQuantity(),stockEntities.get().getObservations()));

        }
        return new StocksDTO(StockDTOList);
    }
    public StocksDTO getStocks() {
        Iterable<StockEntity> stockEntities = stockRepository.findAll();
        List<StockDTO> StockDTOList = new ArrayList<>();

        for (StockEntity stockEntity : stockEntities ){
            Optional<ProductsEntity>ToProductName=productsRepository.findById(stockEntity.getProduct_id());
            Optional<SubsidiariesEntity> ToSubsidiaryName=subsidiariesRepository.findById(stockEntity.getSubsidiary_id()) ;
            Optional<UserEntity>ToUserName=usersRepository.findById(stockEntity.getUser_id());
            StockDTOList.add(
                    new StockDTO(ToProductName.get().getName(), ToSubsidiaryName.get().getName(),ToUserName.get().getUsername(),stockEntity.getQuantity(),stockEntity.getObservations()));

        }
        return new StocksDTO(StockDTOList);
    }
    public ResponseEntity<String> deleteStock(Long id) {
        boolean existStock = stockRepository.existsById(id);
        Optional<StockEntity> stockEntity=stockRepository.findById(id);
        if (existStock) {
            stockRepository.delete(stockEntity.get());
            return new ResponseEntity<>("Operación realizada con éxito", HttpStatus.OK);

        } else {
            throw new BadRequestException("No existe el Id ");
        }

    }
}
