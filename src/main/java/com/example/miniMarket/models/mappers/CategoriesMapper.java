package com.example.miniMarket.models.mappers;

import com.example.miniMarket.models.dtos.NewCategoryDTO;
import com.example.miniMarket.models.entities.CategoriesEntity;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
@Component
public class CategoriesMapper {
    public CategoriesEntity mapCategoryDtoToCategoriesEntity (NewCategoryDTO newCategoryDTO){
        return new CategoriesEntity(newCategoryDTO.getName());
    }
}
