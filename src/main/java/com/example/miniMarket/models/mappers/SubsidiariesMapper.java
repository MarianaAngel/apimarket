package com.example.miniMarket.models.mappers;

import com.example.miniMarket.models.dtos.NewSubsidiariesDTO;
import com.example.miniMarket.models.dtos.NewUserDTO;
import com.example.miniMarket.models.entities.SubsidiariesEntity;
import com.example.miniMarket.models.entities.UserEntity;
import org.springframework.stereotype.Component;

import javax.persistence.Column;

@Component
public class SubsidiariesMapper {

    public SubsidiariesEntity mapSubsidiariesDtoToSubsidiariesEntity (NewSubsidiariesDTO newSubsidiariesDTO){
        return new SubsidiariesEntity(newSubsidiariesDTO.getName(),
                                      newSubsidiariesDTO.getAddress(),
                                       newSubsidiariesDTO.getCity());
    }


}
