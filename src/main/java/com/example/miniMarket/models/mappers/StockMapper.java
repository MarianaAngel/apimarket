package com.example.miniMarket.models.mappers;

import com.example.miniMarket.models.dtos.NewProductsDTO;
import com.example.miniMarket.models.dtos.NewStockDTO;
import com.example.miniMarket.models.entities.ProductsEntity;
import com.example.miniMarket.models.entities.StockEntity;
import org.springframework.stereotype.Component;

@Component
public class StockMapper {
    public StockEntity mapStockDtoToStockEntity(NewStockDTO newStockDTO) {
        return new StockEntity(newStockDTO.getProduct_id(),
                newStockDTO.getSubsidiary_id(), newStockDTO.getUser_id(), newStockDTO.getQuantity(),
                newStockDTO.getObservations(),
                newStockDTO.getCreated());

    }
}
