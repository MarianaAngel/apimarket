package com.example.miniMarket.models.mappers;

import com.example.miniMarket.models.dtos.NewUserDTO;
import com.example.miniMarket.models.entities.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public UserEntity mapUserDtoToUserEntity (NewUserDTO newUserDTO){
        return  new UserEntity(newUserDTO.getFirstName(), newUserDTO.getLastName(), newUserDTO.getAddress(), newUserDTO.getSubsidiary_id(), newUserDTO.getUsername(), newUserDTO.getPassword()) ;


    }
}
