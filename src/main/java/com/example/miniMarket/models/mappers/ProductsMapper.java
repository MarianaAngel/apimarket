package com.example.miniMarket.models.mappers;

import com.example.miniMarket.models.dtos.NewCategoryDTO;
import com.example.miniMarket.models.dtos.NewProductsDTO;
import com.example.miniMarket.models.entities.CategoriesEntity;
import com.example.miniMarket.models.entities.ProductsEntity;
import org.springframework.stereotype.Component;

@Component
public class ProductsMapper {
    public ProductsEntity mapProductsDtoToProductsEntity (NewProductsDTO newProductsDTO){
        return new ProductsEntity(newProductsDTO.getName(),
                newProductsDTO.getObservations(),
                newProductsDTO.getMeasure_unit(),
                newProductsDTO.getUnit_amount(),
                newProductsDTO.getCategory_id(),
                newProductsDTO.getBrand_id());
    }
}
