package com.example.miniMarket.models.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name="categories")
public class CategoriesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @Column(name = "created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    @Column(name = "modified", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date modified;

    public CategoriesEntity() {

    }

    public CategoriesEntity( String name) {

        this.name = name;

    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
