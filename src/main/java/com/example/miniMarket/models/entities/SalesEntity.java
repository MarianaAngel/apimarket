package com.example.miniMarket.models.entities;

import javax.persistence.*;
import java.util.Date;
@Entity(name="sales")
public class SalesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String state;
    private Long subsidiary_id;
    private Long user_id;
    @Column(name = "deleted", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date deleted;
    @Column(name = "created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    @Column(name = "modified", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date modified;

    public SalesEntity() {
    }

    public SalesEntity(Long id, String state, Long subsidiary_id, Long user_id, Date deleted, Date created, Date modified) {
        this.id = id;
        this.state = state;
        this.subsidiary_id = subsidiary_id;
        this.user_id = user_id;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public Long getSubsidiary_id() {
        return subsidiary_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }
}
