package com.example.miniMarket.models.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name="saleItems")
public class SaleItemsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long sale_id;
    private Long product_id;
    private BigDecimal amount;
    @Column(name = "deleted", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date deleted;
    @Column(name = "created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    @Column(name = "modified", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date modified;

    public SaleItemsEntity() {
    }

    public SaleItemsEntity(Long id, Long sale_id, Long product_id, BigDecimal amount, Date deleted, Date created, Date modified) {
        this.id = id;
        this.sale_id = sale_id;
        this.product_id = product_id;
        this.amount = amount;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public Long getSale_id() {
        return sale_id;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }
}
