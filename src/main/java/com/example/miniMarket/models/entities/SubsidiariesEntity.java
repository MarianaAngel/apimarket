package com.example.miniMarket.models.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name="subsidiaries")
public class SubsidiariesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String address;
    private BigDecimal city;
    @Column(name = "created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    @Column(name = "modified", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date modified;
    @Column(name = "deleted", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date  deleted;

    public SubsidiariesEntity() {
    }

    public SubsidiariesEntity(String name, String address, BigDecimal city) {
        this.name = name;
        this.address = address;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public BigDecimal getCity() {
        return city;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(BigDecimal city) {
        this.city = city;
    }
}
