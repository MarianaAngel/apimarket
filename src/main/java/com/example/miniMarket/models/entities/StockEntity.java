package com.example.miniMarket.models.entities;

import javax.persistence.*;
import java.util.Date;

@Entity(name="stock")
public class StockEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long product_id;
    private Long subsidiary_id;
    private Long user_id;
    private Integer quantity;
    private String observations;
    @Column(name = "deleted", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date deleted;
    @Column(name = "created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    @Column(name = "modified", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date modified;

    public StockEntity(Long product_id, Long subsidiary_id, Long user_id,Integer quantity,  String observations, Date created) {
        this.product_id = product_id;
        this.subsidiary_id = subsidiary_id;
        this.user_id = user_id;
        this.quantity=quantity;
        this.observations = observations;
        this.created = created;
    }

    public StockEntity() {
    }

    public Long getProduct_id() {
        return product_id;
    }

    public Long getSubsidiary_id() {
        return subsidiary_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public String getObservations() {
        return observations;
    }

    public Date getCreated() {
        return created;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public void setSubsidiary_id(Long subsidiary_id) {
        this.subsidiary_id = subsidiary_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public StockEntity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
