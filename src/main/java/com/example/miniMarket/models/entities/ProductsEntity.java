package com.example.miniMarket.models.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.DateTimeException;
import java.util.Date;

@Entity(name="products")
public class ProductsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String observations;
    private String measure_unit;
    private BigDecimal unit_amount;
    private Long category_id;
    private Long brand_id;
    @Column(name = "expiration_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date expiration_date;
    @Column(name = "deleted", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date  deleted;
    @Column(name = "created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    @Column(name = "modified", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date modified;


    public ProductsEntity() {

    }

    public ProductsEntity(String name, String observations, String measure_unit, BigDecimal unit_amount, Long category_id, Long brand_id) {

        this.name = name;
        this.observations = observations;
        this.measure_unit = measure_unit;
        this.unit_amount = unit_amount;
        this.category_id = category_id;
        this.brand_id = brand_id;


    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public String getMeasure_unit() {
        return measure_unit;
    }

    public BigDecimal getUnit_amount() {
        return unit_amount;
    }

    public Long getCategory_id() {
        return category_id;
    }

    public Long getBrand_id() {
        return brand_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public void setMeasure_unit(String measure_unit) {
        this.measure_unit = measure_unit;
    }

    public void setUnit_amount(BigDecimal unit_amount) {
        this.unit_amount = unit_amount;
    }

    public void setCategory_id(Long category_id) {
        this.category_id = category_id;
    }

    public void setBrand_id(Long brand_id) {
        this.brand_id = brand_id;
    }
}
