package com.example.miniMarket.models.entities;

import javax.persistence.*;
import java.util.Date;

@Entity(name="users")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    private String address;
    private Long  subsidiary_id;
    private String username;
    private String password;
    @Column(name = "created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    @Column(name = "modified", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date modified;

    public UserEntity() {
    }

    public UserEntity( String firstName, String lastName, String address, Long subsidiary_id, String username, String password) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.subsidiary_id = subsidiary_id;
        this.username = username;
        this.password = password;

    }

    public UserEntity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public Long getSubsidiary_id() {
        return subsidiary_id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setSubsidiary_id(Long subsidiary_id) {
        this.subsidiary_id = subsidiary_id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
