package com.example.miniMarket.models.dtos;

import java.util.Date;

public class NewStockDTO {
    private Long product_id;
    private Long subsidiary_id;
    private Long user_id;
    private Integer quantity;
    private String observations;
    private Date created;

    public NewStockDTO() {
    }

    public NewStockDTO(Long product_id, Long subsidiary_id, Long user_id,Integer quantity, String observations, Date created) {
        this.product_id = product_id;
        this.subsidiary_id = subsidiary_id;
        this.user_id = user_id;
        this.quantity = quantity;
        this.observations = observations;
        this.created = created;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public Long getSubsidiary_id() {
        return subsidiary_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public String getObservations() {
        return observations;
    }

    public Date getCreated() {
        return created;
    }



    public Integer getQuantity() {
        return quantity;
    }

}
