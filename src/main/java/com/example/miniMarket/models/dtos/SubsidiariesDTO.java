package com.example.miniMarket.models.dtos;

import java.util.List;

public class SubsidiariesDTO {
    private List<SubsidiaryDTO> subsidiaries;

    public SubsidiariesDTO() {
    }

    public SubsidiariesDTO(List<SubsidiaryDTO> subsidiaries) {
        this.subsidiaries = subsidiaries;
    }

    public List<SubsidiaryDTO> getSubsidiaries() {
        return subsidiaries;
    }
}
