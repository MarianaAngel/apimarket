package com.example.miniMarket.models.dtos;

import java.util.List;

public class CategoriesDTO {
    private List<CategoryDTO> categoriesDTOS;

    public CategoriesDTO() {
    }

    public List<CategoryDTO> getCategoriesDTOS() {
        return categoriesDTOS;
    }

    public CategoriesDTO(List<CategoryDTO> categoriesDTOS) {

        this.categoriesDTOS = categoriesDTOS;
    }
}
