package com.example.miniMarket.models.dtos;


public class UserDTO {
    private String firstName;
    private String lastName;
    private String address;
    private String subsidiary;
    private String username;
    private String password;

    public UserDTO() {
    }

    public UserDTO(String firstName, String lastName, String address, String subsidiary, String username) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.subsidiary = subsidiary;
        this.username = username;

    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getSubsidiary() {
        return subsidiary;
    }

    public String getUsername() {
        return username;
    }


}
