package com.example.miniMarket.models.dtos;


import javax.persistence.Column;

public class NewUserDTO {
    private String firstName;
    private String lastName;
    private String address;
    private Long  subsidiary_id;
    private String username;
    private String password;

    public NewUserDTO() {
    }

    public NewUserDTO(String firstName, String lastName, String address, Long subsidiary_id, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.subsidiary_id = subsidiary_id;
        this.username = username;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public Long getSubsidiary_id() {
        return subsidiary_id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
