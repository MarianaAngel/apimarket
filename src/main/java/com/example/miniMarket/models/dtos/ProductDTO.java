package com.example.miniMarket.models.dtos;

import java.math.BigDecimal;

public class ProductDTO {
    private String name;
    private String observations;
    private String measure_unit;
    private BigDecimal unit_amount;
    private String category;
    private String brand;

    public ProductDTO(String name, String observations, String measure_unit, BigDecimal unit_amount, String category, String brand) {
        this.name = name;
        this.observations = observations;
        this.measure_unit = measure_unit;
        this.unit_amount = unit_amount;
        this.category = category;
        this.brand = brand;
    }

    public ProductDTO() {

    }



    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public String getMeasure_unit() {
        return measure_unit;
    }

    public BigDecimal getUnit_amount() {
        return unit_amount;
    }

    public String getCategory() {
        return category;
    }

    public String getBrand() {
        return brand;
    }
}
