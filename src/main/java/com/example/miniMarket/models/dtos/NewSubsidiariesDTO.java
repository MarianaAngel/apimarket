package com.example.miniMarket.models.dtos;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.util.Date;

public class NewSubsidiariesDTO {
    private String name;
    private String address;
    private BigDecimal city;

    public NewSubsidiariesDTO() {
    }

    public NewSubsidiariesDTO(String name, String address, BigDecimal city) {
        this.name = name;
        this.address = address;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public BigDecimal getCity() {
        return city;
    }
}
