package com.example.miniMarket.models.dtos;

public class StockDTO {
    private String product;
    private String subsidiary;
    private String user;
    private Integer quantity;
    private String observations;

    public StockDTO() {
    }

    public StockDTO(String product, String subsidiary, String user, Integer quantity, String observations) {
        this.product = product;
        this.subsidiary = subsidiary;
        this.user = user;
        this.quantity = quantity;
        this.observations = observations;
    }

    public String getProduct() {
        return product;
    }

    public String getSubsidiary() {
        return subsidiary;
    }

    public String getUser() {
        return user;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public String getObservations() {
        return observations;
    }
}
