package com.example.miniMarket.models.dtos;

import com.example.miniMarket.models.entities.ProductsEntity;

import java.util.List;

public class ProductsDTO {
    private List<ProductDTO> productsEntities;

    public ProductsDTO() {
    }

    public ProductsDTO(List<ProductDTO> productsEntities) {
        this.productsEntities = productsEntities;
    }

    public List<ProductDTO> getProductsEntities() {
        return productsEntities;
    }
}
