package com.example.miniMarket.models.dtos;

import java.util.List;

public class UsersDTO {
    private List<UserDTO> userDTOS;

    public UsersDTO() {
    }

    public UsersDTO(List<UserDTO> userDTOS) {
        this.userDTOS = userDTOS;
    }

    public List<UserDTO> getUserDTOS() {
        return userDTOS;
    }
}
