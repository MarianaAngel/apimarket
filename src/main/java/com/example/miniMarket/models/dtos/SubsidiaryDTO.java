package com.example.miniMarket.models.dtos;

import java.math.BigDecimal;
import java.util.List;

public class SubsidiaryDTO {
    private String name;
    private String address;
    private BigDecimal city;

    public SubsidiaryDTO() {
    }

    public SubsidiaryDTO(String name, String address, BigDecimal city) {
        this.name = name;
        this.address = address;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public BigDecimal getCity() {
        return city;
    }
}
