package com.example.miniMarket.models.dtos;

public class NewCategoryDTO {
    private String name;

    public NewCategoryDTO() {
    }

    public NewCategoryDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
