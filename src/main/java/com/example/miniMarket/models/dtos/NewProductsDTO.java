package com.example.miniMarket.models.dtos;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.util.Date;

public class NewProductsDTO {

    private String name;
    private String observations;
    private String measure_unit;
    private BigDecimal unit_amount;
    private Long category_id;
    private Long brand_id;
    private Date expiration_date;

    public NewProductsDTO() {
    }

    public NewProductsDTO(String name, String observations, String measure_unit, BigDecimal unit_amount, Long category_id, Long brand_id, Date expiration_date) {
        this.name = name;
        this.observations = observations;
        this.measure_unit = measure_unit;
        this.unit_amount = unit_amount;
        this.category_id = category_id;
        this.brand_id = brand_id;
        this.expiration_date = expiration_date;
    }

    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public String getMeasure_unit() {
        return measure_unit;
    }

    public BigDecimal getUnit_amount() {
        return unit_amount;
    }

    public Long getCategory_id() {
        return category_id;
    }

    public Long getBrand_id() {
        return brand_id;
    }

    public Date getExpiration_date() {
        return expiration_date;
    }
}
