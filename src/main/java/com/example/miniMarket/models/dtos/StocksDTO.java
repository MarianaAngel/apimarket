package com.example.miniMarket.models.dtos;

import java.util.List;

public class StocksDTO {
    private List<StockDTO> stockDTOS;

    public StocksDTO() {
    }

    public StocksDTO(List<StockDTO> stockDTOS) {
        this.stockDTOS = stockDTOS;
    }

    public List<StockDTO> getStockDTOS() {
        return stockDTOS;
    }
}
