package com.example.miniMarket.models.repositories;


import com.example.miniMarket.models.entities.SubsidiariesEntity;
import com.example.miniMarket.models.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;


public interface UsersRepository extends JpaRepository<UserEntity, Long> {

    boolean existsByUsername(String username);

}
