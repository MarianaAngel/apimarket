package com.example.miniMarket.models.repositories;

import com.example.miniMarket.models.entities.BrandsEntities;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandsRepository extends JpaRepository<BrandsEntities, Long> {

}
