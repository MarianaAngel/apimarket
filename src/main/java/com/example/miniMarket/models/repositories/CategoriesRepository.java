package com.example.miniMarket.models.repositories;

import com.example.miniMarket.models.entities.CategoriesEntity;
import com.example.miniMarket.models.entities.SubsidiariesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface  CategoriesRepository extends JpaRepository<CategoriesEntity, Long> {
    List<CategoriesEntity> findAllById(Long Id);

}
