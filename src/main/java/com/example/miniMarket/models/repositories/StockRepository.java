package com.example.miniMarket.models.repositories;

import com.example.miniMarket.models.entities.StockEntity;
import com.example.miniMarket.models.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<StockEntity, Long> {

}
