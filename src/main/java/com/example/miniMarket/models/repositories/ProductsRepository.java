package com.example.miniMarket.models.repositories;

import com.example.miniMarket.models.entities.ProductsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface  ProductsRepository extends JpaRepository<ProductsEntity, Long> {
}
