package com.example.miniMarket.models.repositories;

import com.example.miniMarket.models.entities.SubsidiariesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SubsidiariesRepository  extends JpaRepository<SubsidiariesEntity, Long> {

    List<SubsidiariesEntity> findAllById(Long Id);




}