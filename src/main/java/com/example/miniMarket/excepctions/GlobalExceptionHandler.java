package com.example.miniMarket.excepctions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> defaultErrorHandler(HttpServletRequest request , Exception e){
        e.printStackTrace();
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("Error generico",
                e.getMessage(),"1",request.getRequestURI()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> notFoundHandler (HttpServletRequest request , Exception e){
        return new ResponseEntity<ErrorMessage>(new ErrorMessage(
                "Error Generico",e.getMessage(),
                "2",request.getRequestURI()),
                HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler({BadRequestException.class, HttpMessageNotReadableException.class})
    @ResponseBody
    public ResponseEntity<ErrorMessage> badRequestException (HttpServletRequest request , Exception e){
        return new ResponseEntity<ErrorMessage>(new ErrorMessage(
                "Bad request",e.getMessage(),
                "3",request.getRequestURI()),
                HttpStatus.BAD_REQUEST);
    }

}
